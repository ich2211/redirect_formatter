<?php

namespace Drupal\redirect_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'redirect_link_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "redirect_link_field_formatter",
 *   label = @Translation("Redirect link field formatter"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class RedirectLinkFieldFormatter extends LinkFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $element = parent::viewElements($items, $langcode);

        foreach ($items as $delta => $item) {
            $url = $this->buildUrl($item);
            // append redirect script-tag to element
            $element[$delta]['#attached']['library'][] = 'redirect_formatter/redirect_helper';
            $element[$delta]['#attached']['drupalSettings']['redirect_formatter']['redirect_helper']['url'] = $url->toString();
            // write message that redirect was requested
            $messenger = \Drupal::messenger();
            $messenger->addWarning($this->t('Redirect to ' . $url->toString() . ' requested!'), MessengerInterface::TYPE_WARNING);
        }

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        return [];
    }
}
